package com.devcamp.countryregionapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.model.Country;
import com.devcamp.countryregionapi.service.CountryService;

@CrossOrigin // Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController

public class CountryController {
    @Autowired
    private CountryService countryService;

    // yêu cầu 5 ó đầu ra là ArrayList<Country> thực hiện trả ra danh sách Country
    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = countryService.getAllcountries();
        return allCountry;
    }
    // yêu cầu 6 truyền vào param countryCode từ request, có đầu ra là Country, thực
    // hiện trả ra Country có countryCode tương ứng

    @GetMapping("/country-info")
    public Country getAllCountryInfo(@RequestParam(name = "code", required = true) String countryCode) {
        ArrayList<Country> allCountry = countryService.getAllcountries();

        Country findCountry = new Country();
        for (Country countryElement : allCountry) {
            if (countryElement.getcountryCode().equals(countryCode)) {

                findCountry = countryElement;
            }
        }
        return findCountry;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public Country getRainbow(@PathVariable(name="index") int indexs) {
        return countryService.getCountryIndex(indexs);
    }

}
