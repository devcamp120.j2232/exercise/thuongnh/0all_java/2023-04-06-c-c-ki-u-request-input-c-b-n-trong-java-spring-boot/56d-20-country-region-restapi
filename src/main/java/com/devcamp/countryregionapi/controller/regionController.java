package com.devcamp.countryregionapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.model.Region;
import com.devcamp.countryregionapi.service.RegionService;

 
@RestController
@CrossOrigin
public class regionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info") 
    public Region getRegionInfo(@RequestParam(name="code", required = true) String countryCode){
        System.out.println(countryCode);
       Region findRegion = regionService.filterRegion(countryCode);
        return findRegion;
    }
}
