package com.devcamp.countryregionapi.model;

import java.util.ArrayList;

public class Country {
    private String countryCode;
    private String countryName;
    private ArrayList<Region> regions;
    public Country() {
    }
    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }
    public Country(String countryCode, String countryName, ArrayList<Region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }
    public String getcountryCode() {
        return countryCode;
    }
    public String getcountryName() {
        return countryName;
    }
    public ArrayList<Region> getRegions() {
        return regions;
    }
    public void setcountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public void setcountryName(String countryName) {
        this.countryName = countryName;
    }
    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

    
    
}
