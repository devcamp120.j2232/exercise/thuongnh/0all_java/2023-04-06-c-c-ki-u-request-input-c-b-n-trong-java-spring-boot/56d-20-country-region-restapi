package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;

@Service
public class RegionService {
    // các thành phố của việt nam
    Region hanoi = new Region("HN", "HA NOI");
    Region tpHoChiMinh = new Region("HCM", "Ho CHI MINH");
    Region daNang = new Region("DN", "Da NANG");
    // các thành phố của mỹ
    Region newyorks = new Region("NY", "new yorks");
    Region floria = new Region("FLO", "floria");
    Region texas = new Region("TX", "Texas");

    Region moscow = new Region("MC", "Moscow");
    Region kaluga = new Region("KL", "Kaluga");
    Region saintpeterburg = new Region("SP", "saint peterburg");
    // việt nam  gom các tỉnh thành phố thành 1 arr
    public ArrayList<Region> getRegionsVietNam() {
        ArrayList<Region> regionsVN = new ArrayList<Region>();
        regionsVN.add(hanoi);
        regionsVN.add(tpHoChiMinh);
        regionsVN.add(daNang);
        return regionsVN;

    }
    // mỹ
    public ArrayList<Region> getRegionsUS() {
        ArrayList<Region> regionsUS = new ArrayList<Region>();
        regionsUS.add(newyorks);
        regionsUS.add(floria);
        regionsUS.add(texas);
        return regionsUS;

    }
    // nga 
    public ArrayList<Region> getRegionsMC() {
        ArrayList<Region> regionsMC = new ArrayList<Region>();
        regionsMC.add(moscow);
        regionsMC.add(kaluga);
        regionsMC.add(saintpeterburg);
        return regionsMC;

    }
    // thêm tất cả các thành phố vào 1 array 
    // dồn hết việc vào đây
    public Region filterRegion(String regionCode) {
        ArrayList<Region> regions = new ArrayList<Region>();
        regions.add(hanoi);
        regions.add(tpHoChiMinh);
        regions.add(daNang);

        regions.add(newyorks);
        regions.add(floria);
        regions.add(texas);

        regions.add(moscow);
        regions.add(kaluga);
        regions.add(saintpeterburg);
        Region findRegion = new Region();
        for (Region regionElement : regions){
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
            
        }
        return findRegion;

    }




}
